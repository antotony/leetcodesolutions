class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        strength = [(i, sum(mat[i])) for i in range(len(mat))]
        strength = sorted(strength, key=lambda item: item[1])
        return [weakest[0] for weakest in strength[:k]]

        # the same but faster
        # return [weakest[0] for weakest in sorted([(i, sum(mat[i])) for i in range(len(mat))], key=lambda item: item[1])[:k]]