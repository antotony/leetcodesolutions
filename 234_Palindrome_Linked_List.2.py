from ctypes import pointer
from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        pointer = head
        number_str = ''
        while True:
            number_str += str(pointer.val)
            if pointer.next:
                pointer = pointer.next
            else:
                break
        return number_str[::-1] == number_str


a = ListNode(1, ListNode(2, ListNode(3, ListNode(2, ListNode(1, ListNode(4))))))
b = Solution()
print(b.isPalindrome(a))