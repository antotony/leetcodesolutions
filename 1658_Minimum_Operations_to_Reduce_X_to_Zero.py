from typing import List


class Solution:
    def minOperations(self, nums: List[int], x: int) -> int:
        only_left = x
        left = 0
        right = len(nums) - 1
        min_ops = -1
        while only_left > 0:
            only_left -= nums[left]
            left += 1
            if left > right:
                if only_left > 0:
                    return -1
                elif only_left == 0:
                    return left
        left -= 1
        if only_left == 0:
            min_ops = left + 1

        only_right = x
        while left > -1 or only_right > 0:
            if only_left <= 0:
                only_left += nums[left]
                left -= 1
            if only_left > 0:
                only_right -= nums[right]
                only_left -= nums[right]
                right -= 1
            if only_left == 0:
                min_ops2 = left + 1 + len(nums) - right - 1
                if min_ops2 < min_ops or min_ops == -1:
                    min_ops = min_ops2
        return min_ops

        