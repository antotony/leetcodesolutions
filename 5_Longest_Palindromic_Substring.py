class Solution:
    def longestPalindrome(self, s: str) -> str:
        maxlength = 1
        current_index = left = right = 0
        while current_index < len(s):
            start_palindrom = current_index
            while current_index + 1 < len(s) and s[current_index] == s[current_index + 1]:
                current_index += 1
            end_palindrom = current_index
            current_index += 1
            while start_palindrom - 1 > -1 and end_palindrom + 1 < len(s) and s[start_palindrom - 1] == s[end_palindrom + 1]:
                start_palindrom -= 1
                end_palindrom += 1
            if end_palindrom - start_palindrom + 1 > maxlength:
                maxlength = end_palindrom - start_palindrom + 1
                left = start_palindrom
                right = end_palindrom
        return s[left:right + 1]

        # slover but easier
        # result = s[0]
        # for center in range(len(s[:-1])):
        #     p_l = 0
        #     while center - p_l > -1 and center + 1 + p_l < len(s) and s[center - p_l] == s[center + 1 + p_l]:
        #         p_l += 1
        #     p_l -= 1
        #     if 2 + p_l * 2 > len(result):
        #         result = s[center - p_l : center + 1 + p_l + 1]
        #     p_l = 1
        #     while center - p_l > -1 and center + p_l < len(s) and s[center - p_l] == s[center + p_l]:
        #         p_l += 1
        #     p_l -= 1
        #     if 1 + p_l * 2 > len(result):
        #         result = s[center - p_l : center + p_l + 1]
        # return result

        # rude and inefficient
        # result = ""
        # for start in range(len(s)):
        #     for end in range(len(s[start:])):
        #         substring = s[start:start + end + 1]
        #         if substring == substring[::-1]:
        #             if end + 1 > len(result):
        #                 result = substring
        # return result