class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        def _isMatch(unchecked_s, pat):
            s_iter = 0
            p_iter = 0
            while p_iter < len(pat):
                if p_iter + 1 < len(pat) and pat[p_iter + 1] != '*' or p_iter + 1 == len(pat):
                    if s_iter == len(unchecked_s):
                        return False
                    if pat[p_iter] == '.' or pat[p_iter] == unchecked_s[s_iter]:
                        s_iter += 1
                    else:
                        return False
                    p_iter += 1
                else: # if pat[p_iter + 1] == '*'
                    if p_iter + 2 == len(pat):
                        if pat[p_iter] == '.':
                            return True
                        else:
                            while s_iter < len(unchecked_s) and unchecked_s[s_iter] == pat[p_iter]:
                                s_iter += 1
                            if s_iter == len(unchecked_s):
                                return True
                            else:
                                return False
                    else:
                        return _isMatch(unchecked_s[s_iter:], pat[p_iter + 2:]) or _isMatch(unchecked_s[s_iter:], pat[p_iter] + pat[p_iter:])
            return s_iter == len(unchecked_s)
        
        return(_isMatch(s, p))

sol = Solution()
print(sol.isMatch("aabcbcbcaccbcaabc", ".*a*aa*.*b*.c*.*a*"))
print(sol.isMatch("", "a*"))