class Solution:
    def hasAllCodes(self, s: str, k: int) -> bool:
        checker = [False] * 2**k
        for i in range(len(s) - k + 1):
            checker[int(s[i : i + k], 2)] = True
        return all(checker)


sel = Solution()
print(sel.hasAllCodes('0110', 2))