from typing import List


class Solution:
    def sortEvenOdd(self, nums: List[int]) -> List[int]:
        odd_len_of_nums = bool(len(nums) % 2)
        odd = sorted(nums[1::2])[::-1]
        even = sorted(nums[::2])
        result = [0] * len(nums)
        for i in range(len(even)):
            result[2 * i] = even[i]
            if i < len(odd):
                result[2 * i + 1] = odd[i]
        return result