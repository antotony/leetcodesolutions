class Solution: # best
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        for char in ransomNote:
            if magazine.find(char) > -1:
                magazine = magazine.replace(char, '', 1)
            else:
                return False
        return True