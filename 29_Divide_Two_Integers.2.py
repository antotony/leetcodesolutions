class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        negative_coef = -1 if (dividend < 0) ^ (divisor < 0) else 1
        dividend = abs(dividend)
        divisor = abs(divisor)
        exponent = 31
        counter = 2**exponent
        
        while True:
            if devisor * counter <= dividend and devisor * (counter + 1) > dividend:
                break
            
            if devisor * (counter + 1) == dividend:
                counter += 1
                break

            if devisor * (counter + 1) < dividend:
                counter += 2**exponent
                exponent -= 1

            if devisor * counter > dividend:
                counter -= 2**exponent
                exponent -= 1

        if negative_coef > 0 and counter > 2**31 - 1:
            counter = 2**31 - 1
        return negative_coef * counter