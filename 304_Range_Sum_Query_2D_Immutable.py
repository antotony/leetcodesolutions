from typing import List

class NumMatrix:

    def __init__(self, matrix: List[List[int]]):
        self.matrix = matrix
        self.hashes = {}

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:
        sum_key = ','.join(map(str, [row1, col1, row2, col2]))
        if sum_key not in self.hashes:
            self.hashes[sum_key] = sum([sum(self.matrix[row][col1 : col2 + 1]) for row in range(row1, row2 + 1)])
        return self.hashes[sum_key]