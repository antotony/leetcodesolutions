from typing  import List
from heapq import heappush, heappop


class Solution:
    def furthestBuilding(self, heights: List[int], bricks: int, ladders: int) -> int:
        heap = []
        result_index = 0
        while result_index < len(heights) - 1:
            height_diff = heights[result_index + 1] - heights[result_index]
            if height_diff > 0:
                if bricks - height_diff >= 0:    
                    bricks -= height_diff
                    heappush(heap, -height_diff)
                    result_index += 1
                elif ladders > 0:
                    if heap and -heap[0] > height_diff:
                        bricks -= heappop(heap)
                    else:
                        result_index += 1
                    ladders -= 1
                else:
                    return result_index
            else:
                result_index += 1
        return result_index