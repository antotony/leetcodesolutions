from typing import List


class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:
        problems_flag = False
        n = len(nums)
        for i in range(n - 1):
            if nums[i] > nums[i+1]:
                if problems_flag:
                    return False
                if i == 0:
                    problems_flag = True
                else:
                    if nums[i - 1] <= nums[i + 1]:
                        problems_flag = True
                    else:
                        if i < n - 2:
                            if nums[i] <= nums[i + 2]:
                                problems_flag = True
                            else:
                                return False
                        else:
                            problems_flag = True
        return True
                    