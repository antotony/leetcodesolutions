from typing import List


class Solution:
    def maxScore(self, cardPoints: List[int], k: int) -> int:
        if k == len(cardPoints):
            return sum(cardPoints)
        
        left = k - 1
        selected_sum = sum(cardPoints[:k])
        result_sum = selected_sum
        while left > -1:
            selected_sum -= cardPoints[left]
            selected_sum += cardPoints[-(k - left)]
            result_sum = max(selected_sum, result_sum)
            left -= 1
            
        return result_sum