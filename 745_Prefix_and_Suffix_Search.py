from typing import List


class TreeNode:
    def __init__(self):
        self.children = {}
        self.indexes = []

    def add(self, idx, word):
        if word:
            if word[0] not in self.children:
                self.children[word[0]] = TreeNode()
            self.children[word[0]].indexes.append(idx)
            self.children[word[0]].add(idx, word[1:])
    
    def get_indexes(self, word):
        if not len(word):
            return self.indexes
        return self.children[word[0]].get_indexes(word[1:]) if word[0] in self.children else []


class WordFilter:

    def __init__(self, words: List[str]):
        self.words = words
        self.prefix_tree = TreeNode()
        self.suffix_tree = TreeNode()
        self.results = {}
        for idx, word in enumerate(words):
            self.prefix_tree.add(idx, word)
        for idx, word in enumerate(words):
            self.suffix_tree.add(idx, word[::-1])

    def f(self, prefix: str, suffix: str) -> int:
        results_label = prefix + ':' + suffix
        if results_label in self.results:
            return self.results[results_label]
        
        prefix_indexes = self.prefix_tree.get_indexes(prefix)
        suffix_indexes = self.suffix_tree.get_indexes(suffix[::-1])
        result = list(set(prefix_indexes) & set(suffix_indexes))
        result = max(result) if len(result) > 0 else -1
        self.results[results_label] = result
        return result


# Your WordFilter object will be instantiated and called as such:
# obj = WordFilter(words)
# param_1 = obj.f(prefix,suffix)