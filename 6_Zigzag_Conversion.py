class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s
        result = ''
        for i in range(numRows):
            iterator = i
            from_top_moving = True
            while iterator < len(s):
                result += s[iterator]
                if i < numRows - 1 and i > 0:
                    if from_top_moving:
                        iterator += 2 * (numRows - i) - 2
                    else:
                        iterator += 2 * i
                    from_top_moving = not from_top_moving
                else:
                    iterator += 2 * numRows - 2
        return result