class Solution:
    def minDistance(self, word1: str, word2: str) -> int:  # longer but less memory consumption
        n = len(word1)
        m = len(word2)
        levinstein0 = [0] * (m + 1)
        for i in range(n + 1):
            levinstein1 = [0] * (m + 1)
            for j in range(m + 1):
                if i == 0 and j == 0:
                    levinstein1[0] = 0
                elif j == 0 and i > 0:
                    levinstein1[0] = i
                elif i == 0 and j > 0:
                    levinstein1[j] = j
                elif word1[i - 1] == word2[j - 1]:
                    levinstein1[j] = levinstein0[j - 1]
                else:
                    levinstein1[j] = min(
                        levinstein1[j - 1] + 1,
                        levinstein0[j] + 1,
                    )
            levinstein0 = levinstein1
        return levinstein1[-1]
