from heapq import heappush, heappop
from typing import List


class Solution:
    def isPossible(self, target: List[int]) -> bool:
        heap = []
        targets_sum = sum(target)
        for num in target:
            if num != 1:
                heappush(heap, -num)
            
        while heap:
            current_target = -heappop(heap)
            sum_of_rest = targets_sum - current_target
            if not sum_of_rest:
                return False
            if (current_target - 1) % sum_of_rest == 0:
                targets_sum -= sum_of_rest * (current_target - 1) / sum_of_rest
                continue
            else:
                if not heap:
                    return False
            n = (current_target - (-heap[0])) // sum_of_rest + 1
            remainder = current_target - sum_of_rest * n
            if remainder > 0:
                targets_sum -= sum_of_rest * n
                heappush(heap, -remainder)
            else:
                return False
        return True