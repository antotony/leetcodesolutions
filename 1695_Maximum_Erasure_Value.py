from typing import List


class Solution:
    def maximumUniqueSubarray(self, nums: List[int]) -> int:
        result_sum = 0
        left = 0
        right = 0
        included_nums = {}
        current_sum = 0
        for idx, num in enumerate(nums):
            if num not in included_nums:
                included_nums[num] = idx
                current_sum += num
                right += 1
            else:
                if current_sum > result_sum:
                    result_sum = current_sum
                while num in included_nums:
                    current_sum -= nums[left]
                    del included_nums[nums[left]]
                    left += 1
                current_sum += num
                included_nums[num] = idx
                right += 1
        if current_sum > result_sum:
            result_sum = current_sum
        return result_sum