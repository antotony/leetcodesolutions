class Solution:
    def freqAlphabets(self, s: str) -> str:
        start_chr_value = 96
        two_digits = False
        first = -1
        second = -1
        result = ''
        for char in s[::-1]:
            if char == '#':
                two_digits = True
            elif two_digits:
                if second == -1:
                    second = int(char)
                else: # first == -1:
                    first = int(char)
                    result = chr(start_chr_value + first*10 + second) + result
                    two_digits = False
                    first = -1
                    second = -1
            else:
                result = chr(start_chr_value + int(char)) + result
        return result