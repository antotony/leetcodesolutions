from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def _isPalindrome(self, node_head: Optional[ListNode], node_tail: Optional[ListNode]) -> dict:
        if node_tail.next == None:
            return {
                'isPalindrome': node_head.val == node_tail.val, 
                'node_head': node_head.next
            }
        
        _isPalindrome_res = self._isPalindrome(node_head, node_tail.next)
        return {
            'isPalindrome': _isPalindrome_res['isPalindrome'] and _isPalindrome_res['node_head'].val == node_tail.val, 
            'node_head': _isPalindrome_res['node_head'].next
        }

    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        return self._isPalindrome(head, head)['isPalindrome']


a = ListNode(1, ListNode(2, ListNode(3, ListNode(2, ListNode(1, ListNode(4))))))
b = Solution()
print(b.isPalindrome(a))