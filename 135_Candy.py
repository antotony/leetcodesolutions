from typing import List


# Hard task
# Time: O(n)
# Memory: O(1)

class Solution:
    def candy(self, ratings: List[int]) -> int:
        result = len(ratings)
        prev_candies = 1
        prev_inc_idx = -1
        prev_inc_candies = 1
        for idx, rate in enumerate(ratings[1:]):
            prev = ratings[idx]
            if prev < rate:
                result += prev_candies
                prev_candies += 1
                prev_inc_idx = idx
                prev_inc_candies = prev_candies
            elif prev > rate:
                result += idx - prev_inc_idx - 1
                if prev_inc_candies <= idx - prev_inc_idx:
                    result += 1
                prev_candies = 1
            else:
                prev_inc_idx = idx
                prev_candies = 1
                prev_inc_candies = prev_candies
        return result