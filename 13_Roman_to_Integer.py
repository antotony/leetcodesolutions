class Solution:
    def romanToInt(self, s: str) -> int:
        roman_numbers = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        result = 0
        prev_num = 0
        for char in s:
            num = roman_numbers[char]
            if prev_num < num:
                result += num - 2 * prev_num
            else:
                result += num
            prev_num = num
        return result


if __name__ == '__main__':
    sol = Solution()
    print(sol.romanToInt(input()))