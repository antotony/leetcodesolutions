from ctypes import pointer
from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        len = 1
        pointer = head
        while pointer.next:
            len += 1
            pointer = pointer.next
        center = len // 2
        pointer = head
        for i in range(center):
            pointer = pointer.next
        return pointer

        # this variant is the same, but shorter:
        # center_pointer = head
        # end_pointer = head
        # while end_pointer.next:
        #     center_pointer = center_pointer.next
        #     end_pointer = end_pointer.next
        #     if end_pointer.next:
        #         end_pointer = end_pointer.next
        # return center_pointer