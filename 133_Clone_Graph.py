# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        if not node:
            return None
        
        copied_nodes = {}
        
        def _cloneGraph(cur_node):
            new_node = Node(cur_node.val)
            copied_nodes[cur_node.val] = new_node

            for neighbor in cur_node.neighbors:
                if neighbor.val in copied_nodes:
                    new_node.neighbors.append(copied_nodes[neighbor.val])
                else:
                    new_node.neighbors.append(_cloneGraph(neighbor))
            return new_node
        
        return _cloneGraph(node)