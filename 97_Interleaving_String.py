from functools import cache

# Medium task

# Solution without functools

class Solution:
    def __init__(self):
        self.checked_variants = {}

    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        if len(s1) + len(s2) != len(s3):
            return False
        
        if (s1,s2,s3) in self.checked_variants:
            return self.checked_variants[(s1,s2,s3)]

        right_s1 = len(s1) - 1
        right_s2 = len(s2) - 1
        for s3_idx in range(len(s3) - 1, -1, -1):
            ch = s3[s3_idx]
            if right_s1 >= 0 and ch == s1[right_s1] and right_s2 >= 0 and ch == s2[right_s2]:
                next_results = self.isInterleave(s1[:right_s1], s2[:right_s2 + 1], s3[:s3_idx]) or self.isInterleave(s1[:right_s1 + 1], s2[:right_s2], s3[:s3_idx])
                self.checked_variants[(s1,s2,s3)] = next_results
                return next_results
            elif right_s1 >= 0 and ch == s1[right_s1]:
                right_s1 -= 1
            elif right_s2 >= 0 and ch == s2[right_s2]:
                right_s2 -= 1
            else:
                self.checked_variants[(s1,s2,s3)] = False
                return False
        self.checked_variants[(s1,s2,s3)] = True
        return True

# Solution with functools

class Solution2:
    @cache
    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        if len(s1) + len(s2) != len(s3):
            return False

        right_s1 = len(s1) - 1
        right_s2 = len(s2) - 1
        for s3_idx in range(len(s3) - 1, -1, -1):
            ch = s3[s3_idx]
            if right_s1 >= 0 and ch == s1[right_s1] and right_s2 >= 0 and ch == s2[right_s2]:
                  return self.isInterleave(s1[:right_s1], s2[:right_s2 + 1], s3[:s3_idx]) or self.isInterleave(s1[:right_s1 + 1], s2[:right_s2], s3[:s3_idx])
            elif right_s1 >= 0 and ch == s1[right_s1]:
                right_s1 -= 1
            elif right_s2 >= 0 and ch == s2[right_s2]:
                right_s2 -= 1
            else:
                return False
        return True