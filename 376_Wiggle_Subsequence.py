from typing import List


class Solution:
    def wiggleMaxLength(self, nums: List[int]) -> int:
        if len(nums) <= 1:
            return len(nums)
        plus = None
        prev = nums[0]
        result = 1
        for i in nums[1:]:
            if i != prev:
                if plus != None:
                    if (i - prev > 0) != plus:
                        result += 1
                        plus = not plus
                else:
                    plus = i - prev > 0
                    result += 1
            prev = i
        return result