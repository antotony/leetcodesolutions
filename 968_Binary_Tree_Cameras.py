from typing import Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def __init__(self):
        self.cameras = 0

    def _checkMonitoringAndAddCamera(self, node):
        if not node:
            return 5
        if not node.left and not node.right:
            return 1
        left_state = self._checkMonitoringAndAddCamera(node.left)
        right_state = self._checkMonitoringAndAddCamera(node.right)
        if left_state == 1 or right_state == 1:
            self.cameras += 1
            return 2
        if left_state == 2 or right_state == 2:
            return 3
        return 1

    def minCameraCover(self, root: Optional[TreeNode]) -> int:
        root_state = self._checkMonitoringAndAddCamera(root)
        if root_state == 1:
            self.cameras += 1
        return self.cameras

sol = Solution()
tree = TreeNode(0,TreeNode(0,TreeNode(),TreeNode()))
print('1:', sol.minCameraCover(tree))
print()
sol = Solution()
tree = TreeNode(0,TreeNode(0,TreeNode(0, TreeNode(0, TreeNode(0,right=TreeNode())))))
print('2:', sol.minCameraCover(tree))
print()
sol = Solution()
tree = TreeNode(0,TreeNode(0,TreeNode(), TreeNode(0,TreeNode())), TreeNode(0,TreeNode(0,TreeNode(), TreeNode()), TreeNode(0,TreeNode())))
print('4:', sol.minCameraCover(tree))
print()
sol = Solution()
tree = TreeNode()
print('1:', sol.minCameraCover(tree))
print()
sol = Solution()
tree = TreeNode(0,TreeNode())
print('1:', sol.minCameraCover(tree))
print()
sol = Solution()
tree = TreeNode(0,TreeNode(0,TreeNode(0,TreeNode())))
print('2:', sol.minCameraCover(tree))
print()