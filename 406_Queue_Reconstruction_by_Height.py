from typing import List


class Solution:
    def reconstructQueue(self, people: List[List[int]]) -> List[List[int]]:
        n = len(people)
        sorted_height_people = sorted(people, key=lambda human: (human[0], n - human[1]))
        result = []
        for human in sorted_height_people[::-1]:
            result.insert(human[1], human)
        return result