from typing import List

class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        products.sort()
        result = []
        prefix = ''
        prev = products
        for letter in searchWord:
            prefix += letter
            prev = [x for x in prev if x.startswith(prefix)]
            result.append(prev[:3])
        return result