from typing import List


class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        prev_row = None
        for row in triangle[::-1]:
            if prev_row:
                for i in range(len(row)):
                    row[i] += min(prev_row[i], prev_row[i + 1])
            prev_row = row
        return triangle[0][0]