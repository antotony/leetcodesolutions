# Easy task
# Time: O(n)
# Memory: O(1)

class Solution:
    def fib(self, n: int) -> int:
        fn1 = 0
        fn2 = 1
        if n == 0:
            return fn1
        
        for i in range(n - 1):
            fn1, fn2 = fn2, (fn1 + fn2)
        return fn2

sol = Solution()
print(sol.fib(int(input())))