from typing import List


# Medium task
# Time: O(n)
# Memory: O(n)

class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        nums_set = set(nums)
        
        max_length = 0
        for num in nums_set:
            if num - 1 not in nums_set:
                current_length = 1
                curr_num = num
                while curr_num + 1 in nums_set:
                    current_length += 1
                    curr_num += 1
                max_length = max(max_length, current_length)
        return max_length