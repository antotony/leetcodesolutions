from unittest import result


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        substring = ''
        result = 0
        for letter in s:
            letter_index = substring.find(letter)
            if letter_index > -1:
                if result < len(substring):
                    result = len(substring)
                substring = substring[letter_index + 1:]
            substring += letter
        if result < len(substring):
            result = len(substring)
        return result