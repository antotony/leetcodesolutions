from typing import List


class Solution:
    def longestStrChain(self, words: List[str]) -> int:
        words.sort(key=lambda x: len(x))
        words_dict = {}
        for word in words:
            words_dict[word] = 1
            for i in range(len(word)):
                word_without_letter = word[:i] + word[i+1:]
                if word_without_letter in words_dict:
                    words_dict[word] = max(words_dict[word_without_letter] + 1, words_dict[word])
        return max(words_dict.values())