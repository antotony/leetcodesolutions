from typing import List


class Solution:
    def maxArea(self, h: int, w: int, horizontalCuts: List[int], verticalCuts: List[int]) -> int:
        def max_cut_length(length, cuts):
            max_val = 0
            prev = 0
            for cut in cuts:
                max_val = max(cut - prev, max_val)
                prev = cut
            return max(length - prev, max_val)

        return max_cut_length(h, sorted(horizontalCuts)) * max_cut_length(w, sorted(verticalCuts)) % (10 ** 9 + 7)