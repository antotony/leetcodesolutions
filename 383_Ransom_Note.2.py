class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        for char in ransomNote:
            pos = magazine.find(char)
            if pos > -1:
                magazine = magazine[:pos] + magazine[pos + 1:]
            else:
                return False
        return True