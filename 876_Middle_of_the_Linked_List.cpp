// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        int len = 1;
        ListNode* pointer = head;
        while (pointer->next != nullptr) {
            len += 1;
            pointer = pointer->next;
        }
        int center = len / 2;
        pointer = head;
        for (int i = 0; i < center; ++i) {
            pointer = pointer->next;
        }
        return pointer;
    }
};