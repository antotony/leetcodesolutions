from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        index = 0
        while True:
            the_same_letter = True
            if index == len(strs[0]):
                the_same_letter = False
                break
            letter = strs[0][index]
            for word in strs:
                if index == len(word):
                    the_same_letter = False
                    break
                the_same_letter &= word[index] == letter
                if not the_same_letter:
                    break
            if not the_same_letter:
                break
            else:
                index += 1
        if index:
            return strs[0][:index]
        return ""