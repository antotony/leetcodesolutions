class Solution: # too long
    def divide(self, dividend: int, divisor: int) -> int:
        negative_coef = -1 if (dividend < 0) ^ (divisor < 0) else 1
        dividend = abs(dividend)
        divisor = abs(divisor)
        counter = 0
        if divisor == 1:
            counter = dividend
        elif divisor == 2:
            counter = dividend >> 1
        else:
            while (counter + 1) * divisor <= dividend:
                counter += 1
        if negative_coef > 0:
            if counter > 2**31 - 1:
                counter = 2**31 - 1
        else:
            if counter > 2**31:
                counter = 2**31
        return negative_coef * counter