from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        result_root = ListNode()
        iterator = result_root
        iterator_l1 = l1
        iterator_l2 = l2
        extra = False
        while iterator_l1 or iterator_l2:
            number = 1 if extra else 0
            extra = False
            if iterator_l1:
                number += iterator_l1.val
                iterator_l1 = iterator_l1.next
            if iterator_l2:
                number += iterator_l2.val
                iterator_l2 = iterator_l2.next
            if number > 9:
                extra = True
                number %= 10
            iterator.next = ListNode(number)
            iterator = iterator.next
        if extra:
            iterator.next = ListNode(1)
        return result_root.next