class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        n = len(word1)
        m = len(word2)
        levinstein = [[0] * (m + 1) for _ in range(n + 1)]
        for i in range(len(word1) + 1):
            for j in range(len(word2) + 1):
                if i == 0 and j == 0:
                    levinstein[i][j] = 0
                elif j == 0 and i > 0:
                    levinstein[i][j] = i
                elif i == 0 and j > 0:
                    levinstein[i][j] = j
                elif word1[i - 1] == word2[j - 1]:
                    levinstein[i][j] = levinstein[i - 1][j - 1]
                else:
                    levinstein[i][j] = min(
                        levinstein[i][j - 1] + 1,
                        levinstein[i - 1][j] + 1,
                    )
        return levinstein[-1][-1]
