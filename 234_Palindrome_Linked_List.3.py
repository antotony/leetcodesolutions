from ctypes import pointer
from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution: # best
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        pointer = head
        temp_pointer = None
        while True:
            pointer.prev = temp_pointer
            temp_pointer = pointer
            if pointer.next:
                pointer = pointer.next
            else:
                break

        isPalindrome = True
        pointer2 = head
        while True:
            isPalindrome = isPalindrome and pointer.val == pointer2.val
            if pointer2.next and isPalindrome:
                pointer = pointer.prev
                pointer2 = pointer2.next
            else:
                break
        return isPalindrome


a = ListNode(1, ListNode(2, ListNode(3, ListNode(2, ListNode(1, ListNode(4))))))
b = Solution()
print(b.isPalindrome(a))