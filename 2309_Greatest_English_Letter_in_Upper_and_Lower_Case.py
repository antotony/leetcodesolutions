class Solution:
    def greatestLetter(self, s: str) -> str:
        lower_letter_appearance = set()
        upper_letter_appearance = set()
        for letter in s:
            if letter < 'a':
                upper_letter_appearance.add(letter)
            else:
                lower_letter_appearance.add(letter)
        upper_letter_appearance = sorted(list(upper_letter_appearance))
        iter = len(upper_letter_appearance) - 1
        while iter > -1:
            if upper_letter_appearance[iter].lower() in lower_letter_appearance:
                return upper_letter_appearance[iter]
            iter -= 1
        return ""