from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        l1 = 0 # index for nums1
        m = len(nums1)
        l2 = 0 # index for nums2
        n = len(nums2)
        odd = bool((m + n) % 2)
        for i in range((m + n) // 2 - (0 if odd else 1)):
            if l1 == m:
                l2 += 1
            elif l2 == n:
                l1 += 1
            elif nums1[l1] < nums2[l2]:
                l1 += 1
            else:
                l2 += 1
        if odd:
            if l1 == m:
                return nums2[l2]
            if l2 == n:
                return nums1[l1]
            return min(nums1[l1], nums2[l2])
        else:
            if l1 == m:
                return (nums2[l2] + nums2[l2 + 1]) / 2
            if l2 == n:
                return (nums1[l1] + nums1[l1 + 1]) / 2
            first = min(nums1[l1], nums2[l2])
            if nums1[l1] < nums2[l2]:
                l1 += 1
            else:
                l2 += 1
            if l1 == m:
                return (first + nums2[l2]) / 2
            if l2 == n:
                return (first + nums1[l1]) / 2
            return (min(nums1[l1], nums2[l2]) + first) / 2
