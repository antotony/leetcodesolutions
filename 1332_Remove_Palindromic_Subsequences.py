from unittest import result


class Solution:
    def removePalindromeSub(self, s: str) -> int:
        # def isPalindrome(s):
        #     start_index = 0
        #     end_index = len(s) - 1
        #     while start_index < end_index:
        #         if s[start_index] != s[end_index]:
        #             return False
        #         start_index += 1
        #         end_index -= 1
        #     return True

        # if isPalindrome(s): # is faster, but consumes 1% more memory (I don't really know why)
        if s == s[::-1]:
            return 1
        else:
            return 2

        # the following code is good for a properly set task
        # start_index = 0
        # end_index = len(s) - 1
        # result = 1
        # symmetrical = True
        # last_letter = 'a'
        # while start_index < end_index:
        #     if s[start_index] == s[end_index]:
        #         if not symmetrical:
        #             result += 1
        #             symmetrical = True
        #         last_letter = s[start_index]
        #         start_index += 1
        #         end_index -= 1
        #     else:
        #         symmetrical = False
        #         if s[start_index] == last_letter:
        #             start_index += 1
        #         if s[end_index] == last_letter:
        #             end_index -= 1
        # if start_index == end_index and s[start_index] != last_letter:
        #     result += 1
        # return result
