#include<vector>
using namespace std;

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        int start = 0;
        int end = numbers.size() - 1;
        int my_sum = numbers[start] + numbers[end];
        while (my_sum != target) {
            if (my_sum > target) {
                --end;
            } else {
                ++start;
            }
            my_sum = numbers[start] + numbers[end];
        }
        return vector<int>(start + 1, end + 1);
    }
};