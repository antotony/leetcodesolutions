from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        sorted_nums = sorted(nums1 + nums2)
        return sorted_nums[len(sorted_nums)//2] if len(sorted_nums) % 2 == 1 else (sorted_nums[len(sorted_nums)//2] + sorted_nums[len(sorted_nums)//2 - 1]) / 2