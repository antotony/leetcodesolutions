from typing import List

class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        if n == 1:
            return 1
        elif n < 4:
            return 0
        else:  # 4 - 9
            result = []

            columns = [False] * n
            d1 = [False] * (2 * n - 1)
            d2 = [False] * (2 * n - 1)
            queens = []

            def addQueen():
                i = len(queens)
                for j in range(n):
                    # print(i, j, n, columns, d1, d2)
                    if not (columns[j] or d1[i + j] or d2[n - j + i - 1]):
                        queens.append(j)
                        if len(queens) == n:
                            result.append(queens.copy())
                            queens.pop()
                        else:
                            d1[i + j] = True
                            d2[n - j + i - 1] = True
                            columns[j] = True
                            addQueen()
                            queens.pop()
                            d1[i + j] = False
                            d2[n - j + i - 1] = False
                            columns[j] = False


            addQueen()
            
            result2 = []
            for board in result:
                temp_result = []
                for j in board:
                    row = ['.'] * n
                    row[j] = 'Q'
                    temp_result.append(''.join(row))
                result2.append(temp_result.copy())
            return len(result2)

s = Solution()
print(s.solveNQueens(9))