from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        check = {}
        for idx, num in enumerate(nums):
            if target - num in check:
                return [check[target - num], idx]
            check[num] = idx