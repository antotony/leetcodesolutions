from typing import List


class Solution:
    def maximumUnits(self, boxTypes: List[List[int]], truckSize: int) -> int:
        boxTypes.sort(key=lambda box_type: box_type[1])
        result = 0
        for box_type in boxTypes[::-1]:
            count_of_boxes = min(box_type[0], truckSize)
            truckSize -= count_of_boxes
            result += count_of_boxes * box_type[1]
            if truckSize <= 0: break
        return result